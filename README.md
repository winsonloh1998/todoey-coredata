# Todoey-CoreData iOS13 (AppBrewery Course)
A To-Do iOS application that stores data using CoreData

## What I Have Learned
- UserDefaults
- CoreData

## Screenshot - Category
![Category Screenshot](https://bitbucket.org/winsonloh1998/todoey-coredata/raw/92ccad31952493419c3af9b3ff7f33f922232dae/Screenshot/Category.png)

## Screenshot - Task
![Task Screenshot](https://bitbucket.org/winsonloh1998/todoey-coredata/raw/92ccad31952493419c3af9b3ff7f33f922232dae/Screenshot/Task.png)

## Screenshot - Searched Task
![Searched Task Screenshot](https://bitbucket.org/winsonloh1998/todoey-coredata/raw/5ec0930f35f55fb201f3f8edef9c63a7b9d9f725/Screenshot/SearchedTask.png)